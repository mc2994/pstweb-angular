import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { GridOptions } from 'ag-grid-community';
import { TabControllerService } from 'src/app/core/services/tab-controller.service';
import { User } from 'src/app/shared/models/user';
import { UserAuthenticationService } from 'src/app/core/authentication/user-authentication.service';
import { UserDetail } from 'src/app/shared/models/user-detail';
import { Task } from 'src/app/shared/models/task';
import { Applications } from 'src/app/shared/models/applications';
import { CDSResponse } from 'src/app/shared/models/cds-response';
import * as Config from '../../../configs/config-app';
import { GridConfig } from 'src/app/shared/models/grid-config';
import { GridDataRequest } from 'src/app/shared/models/grid-data-request';
import { GridAttributeConfig } from 'src/app/shared/models/grid-attribute-config';
import { GridDataResponse } from 'src/app/shared/models/grid-data-response';
import { GridActionConfig } from 'src/app/shared/models/grid-action-config';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-tab-controller',
  templateUrl: './tab-controller.component.html',
  styleUrls: ['./tab-controller.component.scss']
})
export class TabControllerComponent implements OnInit {

  public gridOptions: Partial<GridOptions>;
  public gridApi;
  public gridColumnApi;
  public maxConcurrentDatasourceRequests;
  public infiniteInitialRowCount;
  public rowData: CDSResponse = new CDSResponse();
  public displayAnouncements:boolean = false;
  public showDataTable:boolean = false;
  public isFormVisible:boolean = false;
  public isShowModalForm:boolean = false;
  public skeletonDataTable: boolean = false;
  public skeletonAnnouncements: boolean = false;
  public user: User = new User();
  public userResponse: CDSResponse = new CDSResponse();
  public userInfo: UserDetail = new UserDetail();
  public actions: Map<string, GridActionConfig> = new Map<string, GridActionConfig>();
  public buttonIcon: Map<string, string> = new Map<string, string>();
  public tableName = '';
  public columnDefs: any[] = [];
  public USER_LOGO = Config.USER_LOGO;
  public rows: any[] = [];
  public userName: string = '';
  public addForm: FormGroup;
  public rootGridConfig: Array<GridConfig> = new Array<GridConfig>();
  public selectedSubMenuIndex:number = 0;
  public selectedMenuIndex:number = 0;

  @ViewChild('closebutton', {static:true}) closebutton: ElementRef;;

  constructor(private tabservice: TabControllerService,
    private authentication: UserAuthenticationService,
    private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.skeletonAnnouncements = true;
    this.getMenuandActions();
    this.createButtonIcons();   

    this.addForm = this.formBuilder.group({

    });
    this.getAnnouncements(1);
  }

  public getDatafromService(task: Task) {
    event.preventDefault();
    this.initiaLizeGridComponent();
    this.showDataTable = false;
    this.skeletonDataTable = true;
    this.displayAnouncements = false;
    this.isShowModalForm = false;   
    this.selectedSubMenuIndex = task.taskId;
    this.fetchRecords(task.taskId);
  }

  private fetchRecords(taskId: number): Array<GridConfig> {
    this.tabservice.fetchRecordsFromDB(taskId)
      .subscribe((data: CDSResponse) => {
          this.rowData = data;
          this.rootGridConfig = this.rowData.result["rootGridConfig"]; 
          let request: GridDataRequest = this.buildGridRequest(this.rootGridConfig, ActionType.READ_MANY);
          this.populateDataTable(request);
      });
      return this.rootGridConfig;
  }

  private getAnnouncements(taskId: number): Array<GridConfig> {
    this.tabservice.fetchRecordsFromDB(taskId)
      .subscribe((data: CDSResponse) => {
          this.rowData = data;
          this.rootGridConfig = this.rowData.result["rootGridConfig"]; 
          let request: GridDataRequest = this.buildGridRequest(this.rootGridConfig, ActionType.READ_MANY);
          this.buildCDSAnnouncements(request);
      });
      return this.rootGridConfig;
  }


  private buildGridRequest(gridConfig: Array<GridConfig>, actionType: string): GridDataRequest {
    let request: GridDataRequest = new GridDataRequest();
    request.requestor.adId = this.authentication.getLoggedInUserName();
    gridConfig.forEach(action => {
            request.gridInfo.gridName = action.gridName;
             for (const [key, value] of Object.entries(action.gridActions)) {
                 if(key==="create-one" || key==="update-one"){
                      value.data_target = "#modalRegisterForm";
                      value.data_toggle = "modal"
                 }
                  this.actions.set(key,value);

                  if(actionType===ActionType.CREATE_ONE){               
                      request.gridInfo.actionType = "Create";
                      request.gridInfo.actionCode = ActionType.CREATE_ONE;    
                      request.transactionRecordDetails = this.addForm.value;                              
                  }else if(actionType===ActionType.DELETE_ONE){
                      request.gridInfo.actionType = "Delete";
                      request.gridInfo.actionCode = ActionType.DELETE_ONE;
                  }else if(actionType===ActionType.READ_MANY){
                      request.gridInfo.actionType = "Select";
                      request.gridInfo.actionCode = ActionType.READ_MANY;
                      
                  }else if(actionType===ActionType.UPDATE_ONE){
                      request.gridInfo.actionType = "Update";
                      request.gridInfo.actionCode = ActionType.UPDATE_ONE;                    
                  }
                  request.gridRecordDetails[Config.PST_RSRV_VAR_USERID] =  request.requestor.adId;
                  request.gridRecordDetails[Config.PST_RSRV_VAR_GRIDTABLENAME] =  action.tableName;                 
                  request.pageInfo.requestedPage = 1;
                  request.pageInfo.returnedPage = 0;
                  request.pageInfo.lastPageIndicator = "N";
                  request.token = sessionStorage.getItem(Config.TOKEN);                  
          }        
          this.buildTableHeader(action.gridAttr);
          this.tableName = action.gridTitle;
        });
      
    return request;
  }

  public getMenuandActions() {
    this.tabservice.getMenuActionsFromDB()
        .subscribe((response: CDSResponse) => {
            this.userResponse = new CDSResponse({
                result: response.result,
                applications: response.result["applications"] as Array<Applications>,
                username: response.result['username']
            });
            this.userName = this.userResponse['username']
            let applications: Array<Applications> = this.userResponse.result['applications'];
            applications.forEach(app=>{
                if(app.appId==1) applications.shift();
            });       
            sessionStorage.setItem(Config.TOKEN, response.result[Config.TOKEN]);
            this.getAnnouncements(1);
      }, ()=>{
               
      });
  }

  public populateDataTable(request: GridDataRequest) {
    this.tabservice.populateDataTableService(request)
        .subscribe((rowData: CDSResponse) => {
            let gridResponse: GridDataResponse = rowData.result as GridDataResponse;
            this.buildRowData(gridResponse);         
            this.displayAnouncements = false;        
            this.showDataTable = true;
            this.skeletonDataTable = false;
      }, ()=>{
            
      });
  }

  public buildCDSAnnouncements(request: GridDataRequest) {
    this.tabservice.populateDataTableService(request)
        .subscribe((rowData: CDSResponse) => {
            let gridResponse: GridDataResponse = rowData.result as GridDataResponse;
            this.buildRowData(gridResponse);         
            this.displayAnouncements = true;
            this.skeletonAnnouncements = false;
      }, ()=>{
            
      });
  }

  private buildTableHeader(gridAttr: Array<GridAttributeConfig>) {
    this.columnDefs = [];
    if (gridAttr!=null) {
        gridAttr.forEach(value => {
        this.columnDefs.push(
            { 
              headerName: value.displayTxt, 
              field: value.dbFieldName,
              sortable: true,
              filter: true,
              checkboxSelection: function(params) {
                const displayedColumns = params.columnApi.getAllDisplayedColumns();
                return displayedColumns[0] === params.column;
            },
            });
      });
    }
  }

  private buildRowData(response: GridDataResponse) {
    this.rows = [];
    if (response != null) {
        response.gridResultDetails.forEach(row => {
        this.rows.push(row);
      });
    }
}

  private createButtonIcons() {
    this.buttonIcon.set('Bulk Load', 'fa fa-upload');
    this.buttonIcon.set('Export', 'fa fa-upload');
    this.buttonIcon.set('Filter', 'fa fa-filter');
    this.buttonIcon.set('Refresh', 'fa fa-refresh');
    this.buttonIcon.set('Edit', 'fa fa-pencil');
    this.buttonIcon.set('Add', 'fa fa-plus');
    this.buttonIcon.set('Delete', 'fa fa-trash');
  }

  private initiaLizeGridComponent() {
    this.gridOptions = {
         headerHeight: Config.HEADER_HEIGHT,
         rowHeight: Config.ROW_HEIGHT,
         paginationPageSize: Config.PAGINATION_PAGE_SIZE,
         rowModelType: Config.ROW_MODEL_TYPE       
    };
  }

  public loadComponent(actionType: string){
    switch(actionType){
      case ActionType.CREATE_ONE:
        this.addRecord();
        break;
      case ActionType.DELETE_ONE:
        this.deleteRecord();
        break;
      case ActionType.READ_MANY:
        this.refresh();
        break;
      case ActionType.UPDATE_ONE:
        this.updateRecord();
        break;
      default:
        break;
    }

  }

  private addRecord(){
     //this.showDataTable = false;
     if(this.columnDefs.length<0){
       return;
     }
    this.columnDefs.forEach(col=>{
      console.log("Field: ", col.field)
      this.addForm.addControl(col.field , new FormControl())
    });
    this.isShowModalForm = true;
  }

  private updateRecord(){
  //   if(this.addForm.invalid){
  //     return;
  //   }

  // let request: GridDataRequest = this.buildGridRequest(this.rootGridConfig, ActionType.CREATE_ONE);
  // request.gridRecordDetails = this.createGrid(Config.PST_RSRV_VAR_USERID, request.requestor.adId);
  // request.gridRecordDetails = this.createGrid(Config.PST_RSRV_VAR_GRIDTABLENAME, this.tableName);
  // request.gridRecordDetails = this.createGrid('request', this.addForm.value);
  // request.filterDetails.set('sdsadsadas', 1111);
  // this.populateDataTable(request);
  
  }

  private deleteRecord(){
   
  }

  private refresh(){
    this.showDataTable = true;
    this.isFormVisible = false;
    let request: GridDataRequest = this.buildGridRequest(this.rootGridConfig, ActionType.READ_MANY);
    this.populateDataTable(request);
  }

  public getField(field:string) {    
    return this.addForm.get(field);  
  }

  public saveRecord(){
      if(this.addForm.invalid){
        return;
      }
    let request: GridDataRequest = this.buildGridRequest(this.rootGridConfig, ActionType.CREATE_ONE);
 
    this.populateDataTable(request);
    this.isFormVisible = false;
    this.showDataTable = true
    this.closebutton.nativeElement.click();
      //let request: GridDataResponse = this.addForm.value as GridDataResponse;      
  }

  getMainMenuIndex(index:number){
    this.selectedMenuIndex = index;
    this.selectedSubMenuIndex = 0;
  }
  
}

enum ActionType{
  CREATE_ONE = "create-one",
	DELETE_ONE = "delete-one",
	UPDATE_ONE = "update-one",
  READ_MANY = "read-many" 
}