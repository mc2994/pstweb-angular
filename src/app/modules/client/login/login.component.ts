import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/shared/models/user';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserAuthenticationService } from 'src/app/core/authentication/user-authentication.service';
import * as Config from '../../../configs/config-app';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public addForm: FormGroup;
  public user: User = new User();
  public APP_LOGO = Config.APP_LOGO;
  public APP_NAME = Config.APP_NAME;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private userAuthentication: UserAuthenticationService) {

      // temporary for now
        if (this.userAuthentication.isUserLoggedIn()) {
            this.router.navigate(['/prod-support']);
        }
     }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  get f() { return this.addForm.controls; }

  onSubmit() {
    if (this.addForm.invalid) {
      return;
    }
    this.user = this.addForm.value;
    this.userAuthentication.registerSuccessfulLogin(this.user);
    this.router.navigate(['/prod-support']);

    // to be implemented in future for authentication
    // this.userAuthentication.userLoginService(this.addForm.value)
    //   .subscribe(data => {
    //     this.userAuthentication.registerSuccessfulLogin(this.user.username, this.user.password);
    //     this.router.navigate(['/prod-support']);
    //   }, error => {
    //     console.error("Error: " + error);
    //     return;
    //   })
  }
}
