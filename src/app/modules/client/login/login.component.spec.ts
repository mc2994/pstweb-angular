import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create username and password field', () => {
    const localFixture = TestBed.createComponent(LoginComponent);
    const app = localFixture.debugElement.nativeElement;
    expect(app.querySelector('#username')).toBeTruthy();
    expect(app.querySelector('#password')).toBeTruthy();
  });
});
