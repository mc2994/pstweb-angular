import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './modules/client/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { TabControllerComponent } from './modules/client/tab-controller/tab-controller.component';
import { AgGridModule } from 'ag-grid-angular';
import { TabControllerService } from './core/services/tab-controller.service';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './core/header/header/header.component';
import { FooterComponent } from './core/footer/footer/footer.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpInterceptorService } from './core/interceptors/http-interceptor.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ForbiddenComponent } from './modules/forbidden/forbidden.component';
import { NotFoundComponent } from './modules/not-found/not-found.component';
import { ErrorHandlerInterceptor } from './core/interceptors/error-handler.interceptor';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TabControllerComponent,
    HeaderComponent,
    FooterComponent,
    ForbiddenComponent,
    NotFoundComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AgGridModule.withComponents([]),
    FormsModule,
    NgxPaginationModule,
    NgxSkeletonLoaderModule
  ],
  providers: [
    TabControllerService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
