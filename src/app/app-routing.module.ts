import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './modules/client/login/login.component';
import { TabControllerComponent } from './modules/client/tab-controller/tab-controller.component';
import { AuthGuardService } from '../app/core/guards/auth-guard.service';
import { ForbiddenComponent } from './modules/forbidden/forbidden.component';
import { NotFoundComponent } from './modules/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  { path: 'prod-support',
    component: TabControllerComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: '401',
    component: ForbiddenComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
