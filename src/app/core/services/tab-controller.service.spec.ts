import { TestBed } from '@angular/core/testing';

import { TabControllerService} from './tab-controller.service';

describe('TabControllerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TabControllerService = TestBed.get(TabControllerService);
    expect(service).toBeTruthy();
  });
});
