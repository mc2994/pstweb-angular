import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { UserDetail } from 'src/app/shared/models/user-detail';
import { CDSResponse } from 'src/app/shared/models/cds-response';
import * as Config from '../../configs/config-app';
import { GridDataRequest } from 'src/app/shared/models/grid-data-request';
import { UserAuthenticationService } from '../authentication/user-authentication.service';

@Injectable({
  providedIn: 'root'
})
export class TabControllerService {

  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    Accept: 'application/json'
  });

  private username:string = "";
  
  constructor(private http: HttpClient, private userAuthentication: UserAuthenticationService) {
      this.username = this.userAuthentication.getLoggedInUserName();
   }  

  public getMenuActionsFromDB(): Observable<CDSResponse> {
    return this.http.get<CDSResponse>(`${environment.configAppUrl}/user/${this.username}/authorization/${environment.authorization}`, { headers: this.headers });
  }

  public getTaskNameByTaskID() {
    return this.http.get<UserDetail>(`${environment.configAppUrl}/user/${this.username}/authorization/${environment.authorization}`, { headers: this.headers });
  }

  public populateDataTableService(request: GridDataRequest): Observable<CDSResponse> {
    return this.http.post<CDSResponse>(`${environment.gridDataUrl}/griddata/transaction`, request, { headers: this.headers });
  }

  public fetchRecordsFromDB(taskId: any): Observable<CDSResponse> {
    return this.http.get<any>(`${environment.configAppUrl}/user/${this.username}/task/${taskId}/token/${sessionStorage.getItem(Config.TOKEN)}`, { headers: this.headers });
  }
}
