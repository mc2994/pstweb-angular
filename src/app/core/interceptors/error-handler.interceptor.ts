import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UNAUTHORIZED, BAD_REQUEST, FORBIDDEN } from 'http-status-codes';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerInterceptor implements HttpInterceptor {
  constructor(
    // private toasterService: ToasterService,
    private router: Router
  ) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next
      .handle(request)
      .pipe(
        catchError(error =>
          this.errorHandler(error)));

  }

  private errorHandler(response: HttpResponse<any>): Observable<HttpResponse<any>> {
    if (!environment.production) {
        console.error('Request error ' + JSON.stringify(response));
    }

    const httpErrorCode = response.status;
    switch (httpErrorCode) {
      case UNAUTHORIZED:
        this.router.navigateByUrl('/auth/login');
        break;
      case FORBIDDEN:
        this.router.navigateByUrl('/auth/403');
        break;
      case BAD_REQUEST:
        // this.showError(error.message);
        break;
      default:
      // this.toasterService.pop('error', appToaster.errorHead, response['message']);
    }

    throw throwError(response);
  }
}
