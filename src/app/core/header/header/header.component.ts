import { Component, OnInit, Input } from '@angular/core';
import * as Config from '../../../configs/config-app';
import { Router } from '@angular/router';
import { UserAuthenticationService } from '../../authentication/user-authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public SPRINT_LOGO = Config.SPRINT_LOGO;
  public APP_LOGO = Config.APP_LOGO;
  @Input() public userName: string = '';

  constructor(private router: Router,
              private authentication: UserAuthenticationService) { }

  ngOnInit() {
  }

  public logout() {
    this.authentication.logout();
    this.router.navigate(['/login']);
  }
}
