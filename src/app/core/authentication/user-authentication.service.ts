import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from 'src/app/shared/models/user';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import * as Config from '../../configs/config-app';

@Injectable({
  providedIn: 'root'
})
export class UserAuthenticationService {

  private headers = new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json'
});

  public username: string;
  public password: string;

  constructor(private http: HttpClient) { }

  userLoginService(user: User): Observable<User> {
    return this.http.post<User>(`${environment.userUrl}/userlogin`, user, { headers: this.headers });
  }

  registerSuccessfulLogin(user: User) {
    sessionStorage.setItem(Config.USER_NAME_SESSION_ATTRIBUTE_NAME, user.username);
  }

  logout() {
    sessionStorage.removeItem(Config.USER_NAME_SESSION_ATTRIBUTE_NAME);
    sessionStorage.removeItem(Config.TOKEN);
    this.username = null;
    this.password = null;
  }

  isUserLoggedIn() {
    const user = sessionStorage.getItem(Config.USER_NAME_SESSION_ATTRIBUTE_NAME);
    if (user === null) { return false; }
    return true;
  }

  getLoggedInUserName() {
    const user = sessionStorage.getItem(Config.USER_NAME_SESSION_ATTRIBUTE_NAME);
    if (user === null) { return ''; }
    return user;
  }
}
