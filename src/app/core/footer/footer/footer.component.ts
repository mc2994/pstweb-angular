import { Component, OnInit } from '@angular/core';
import * as Config from '../../../configs/config-app';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public FOOTER_CONTENT = Config.FOOTER_CONTENT;
  public FOOTER_LOGO = Config.FOOTER_LOGO;

  constructor() { }

  ngOnInit() { }

}
