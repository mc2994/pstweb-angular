import { Task } from './task';

export class Applications {
    public appId: number;
    public appName: string;
    public tabPosition: number;
    public url: string;
    public tasks: Array<Task>;

    constructor(values: object = {}) {
        Object.assign(this, values);
        this.tasks = new Array<Task>();
    }
}
