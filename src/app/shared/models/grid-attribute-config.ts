export class GridAttributeConfig {
    public gridName: string;
    public colId: number;
    public displayTxt: string;
    public dataType: string;
    public dbFieldName: string;
    public primaryKeyCd: string;
    public modifyCd: string;
    public visibleCd: string;
    public filterCd: string;
    public maxLength: number;
    public minLength: number;
    public maxValue: number;
    public minValue: number;
    public nullableCd: string;
    public inputSrc: string;
    public inputRef: string;
    public childKeyCd: string;

    constructor() {

    }
}
