export class GridActionConfig {
    public gridName: string;
	public actionSeq: number;
	public actionSql: string;
	public gridActionCd: string;
	public gridActionType: string;
	public buttonDisplayText: string;
	public buttonPosition: string;
	public orderByTxt: string;
	public customEndpoint: string;
	public data_target: string;
	public data_toggle: string;

    constructor() {

    }
}
