export class Task {
    public taskId: number;
    public appId: number;
    public taskName: string;
    public dispOrder: number;
    public noteFlag: string;
    public permission: string;

    constructor(values: object = {}) {
        Object.assign(this, values);
    }
}
