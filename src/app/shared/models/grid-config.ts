import { GridActionConfig } from './grid-action-config';
import { GridAttributeConfig } from './grid-attribute-config';

export class GridConfig {
   public taskId: number;
   public gridName: string;
   public gridTitle: string;
   public orderNbr: number;
   public tableName: string;
   public parentName: string;
   public exportFlag: string;
   public delTriggerFlag: string;
   public filterFlag: boolean;
   public childGridConfig: Array<GridConfig>;
   public gridActions: Map<string, GridActionConfig>;
   public gridAttr: Array<GridAttributeConfig>;
   public psMapIdx: Array<number>;

    constructor() {
        this.gridAttr = new Array<GridAttributeConfig>();
    }
}
