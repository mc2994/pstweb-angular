import { RequestorInfo } from './requestor-info';
import { GridInfo } from './grid-info';
import { PaginationInfo } from './pagination-info';
import { ParamMap } from './param-map';

export class GridDataRequest{
    public requestor: RequestorInfo;
	public gridInfo: GridInfo;
	public gridRecordDetails: {};
	public transactionRecordDetails: {};
	public activeRecordDetails: ParamMap;
	public filterDetails: ParamMap;
	public pageInfo: PaginationInfo;
    public token: string;
    
    constructor(values: object = {}) {
        Object.assign(this, values);     
        this.requestor = new RequestorInfo();
        this.gridInfo = new GridInfo();
        this.gridRecordDetails = {};
        this.transactionRecordDetails = {};
        this.activeRecordDetails  = new ParamMap();
        this.filterDetails  = new ParamMap();
        this.pageInfo = new PaginationInfo();
        this.token = "";
    }
}