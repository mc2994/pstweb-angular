import { Applications } from './applications';

export class User {
    public userId = '';
    public firstName = '';
    public lastName = '';
    public username = '';
    public password = '';
    public orgName = '';
    public activeFlag = false;
    public applications: Array<Applications> = new Array<Applications>();

    constructor(values: object = {}) {
        Object.assign(this, values);
    }
}
