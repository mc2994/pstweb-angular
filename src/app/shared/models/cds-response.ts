import { Error } from './error';

export class CDSResponse {
    public id: string;
    public requestURI: string;
	public result: Object;
    public error: Error;

    constructor(values: object = {}) {
        Object.assign(this, values);
    }
}
