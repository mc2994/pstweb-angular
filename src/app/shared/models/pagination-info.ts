export class PaginationInfo{
    public requestedPage: number = 1;
	public returnedPage: number = 0;
    public lastPageIndicator: string = "N";
    
    constructor(){

    }
}