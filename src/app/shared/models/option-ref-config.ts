import { Options } from './options';

export class OptionRefConfig {
    public optRefKey: string;
    public options: Array<Options>;

    constructor() {

    }
}
