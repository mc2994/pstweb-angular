import { RequestorInfo } from './requestor-info';
import { GridInfo } from './grid-info';
import { PaginationInfo } from './pagination-info';
import { ParamMap } from './param-map';

export class GridDataResponse{
    public  requestor: RequestorInfo;
	public  gridInfo: GridInfo;
	public  gridRecordDetails: ParamMap;
	public  transactionRecordDetails: ParamMap;
	public  activeRecordDetails: ParamMap;
	public  filterDetails: ParamMap;
	public  gridResultDetails: Array<Map<string, Object>>;
    public  pageInfo: PaginationInfo;

    constructor(){
        this.requestor = new RequestorInfo();
        this.gridInfo = new GridInfo();
        this.gridRecordDetails = new ParamMap();
        this.transactionRecordDetails = new ParamMap();
        this.activeRecordDetails = new ParamMap();
        this.filterDetails = new ParamMap();
        this.gridResultDetails = new Array<Map<string, Object>>();
        this.pageInfo = new PaginationInfo();
    }
    
}