import { GridConfig } from './grid-config';
import { OptionRefConfig } from './option-ref-config';

export class TaskConfig {
    public adid: string;
	public taskId: number;
	public rootGridConfig: Array<GridConfig>;
    public refValues: Array<OptionRefConfig>;

    constructor() {

    }
}
