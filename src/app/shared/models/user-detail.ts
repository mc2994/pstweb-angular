import { User } from './user';
import { Applications } from './applications';

export class UserDetail {
    public userInfo: User;
    public applications: Array<Applications> ;

    constructor() {
        this.userInfo = new User();
        this.applications = new Array<Applications>();
    }
}
