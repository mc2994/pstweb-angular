const host:string = "http://localhost";
const port:string  = "8080";

export const environment = {
  production: false,
  userUrl : `${host}:${port}/rest/user`,
  configAppUrl : `${host}:${port}/rest/configapp`,
  gridDataUrl : `${host}:${port}/rest`,
  authorization: 'PST'
};